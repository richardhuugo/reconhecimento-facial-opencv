/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reconhecimento;

import java.awt.event.KeyEvent;
import java.util.Scanner;
import org.bytedeco.javacpp.DoublePointer;
import org.bytedeco.javacpp.IntPointer;
import org.bytedeco.javacpp.opencv_core;
import static org.bytedeco.javacpp.opencv_core.FONT_HERSHEY_PLAIN;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Point;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_face.FaceRecognizer;
import static org.bytedeco.javacpp.opencv_face.createEigenFaceRecognizer;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import static org.bytedeco.javacpp.opencv_imgproc.COLOR_BGRA2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.putText;
import static org.bytedeco.javacpp.opencv_imgproc.rectangle;
import static org.bytedeco.javacpp.opencv_imgproc.resize;
import org.bytedeco.javacpp.opencv_objdetect;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.OpenCVFrameConverter;
import org.bytedeco.javacv.OpenCVFrameGrabber;

/**
 *
 * @author Phugo
 */
public class Reconhecimento {
    public static void main(String args[]) throws FrameGrabber.Exception, InterruptedException{
        KeyEvent  tecla = null;
        OpenCVFrameConverter.ToMat convertMat = new OpenCVFrameConverter.ToMat();
        OpenCVFrameGrabber camera = new OpenCVFrameGrabber(0);
        String[] pessoas  = {"", "Hugo", "Bruno"};
        camera.start();
        
        opencv_objdetect.CascadeClassifier detectorFace = new opencv_objdetect.CascadeClassifier("src\\recursos\\haarcascade-frontalface-alt.xml");
            
        FaceRecognizer reconhecedor = createEigenFaceRecognizer();
        reconhecedor.load("src\\recursos\\classificarEigenFaces.yml");
        CanvasFrame cFrame = new CanvasFrame("Preview", CanvasFrame.getDefaultGamma() / camera.getGamma());
        Frame frameCapturado = null;
        opencv_core.Mat imageColorida  = new opencv_core.Mat();
  
        
        while((frameCapturado = camera.grab()) !=  null){
            imageColorida = convertMat.convert(frameCapturado);
            opencv_core.Mat imagemCinza = new opencv_core.Mat();
            cvtColor(imageColorida,imagemCinza,COLOR_BGRA2GRAY);
            opencv_core.RectVector facesDetectada = new opencv_core.RectVector();
            detectorFace.detectMultiScale(imagemCinza, facesDetectada,1.1, 1,0,new opencv_core.Size(150,150), new opencv_core.Size(500,500));
         
            for(int i=0; i< facesDetectada.size(); i++ ){
                opencv_core.Rect dadosFace = facesDetectada.get(0);
                rectangle(imageColorida, dadosFace, new opencv_core.Scalar(0,0,255,0));
                opencv_core.Mat  faceCapturada = new opencv_core.Mat(imagemCinza,dadosFace);               
                resize(faceCapturada, faceCapturada, new opencv_core.Size(160,160));
                IntPointer rotulo = new IntPointer(1);
                DoublePointer confianca = new DoublePointer(1);
                reconhecedor.predict(faceCapturada,rotulo, confianca);
                int predicao = rotulo.get(0);
                String nome;
                if(predicao == -1){
                    nome = "Desconhecido";
                }else{
                    nome = pessoas[predicao]+ " - "+ confianca.get(0);
                }
                
                int x= Math.max(dadosFace.tl().x() - 10 , 0);
                int y = Math.max(dadosFace.tl().y() - 10 , 0);
                putText(imageColorida, nome, new Point(x,y), FONT_HERSHEY_PLAIN, 1.4, new Scalar(0,255,0,0));
            }
          
            if(cFrame.isVisible()){
                cFrame.showImage(frameCapturado);
                
            }
         
        }
        cFrame.dispose();
        camera.stop();
        
        
    }
}
